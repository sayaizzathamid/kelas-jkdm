<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    use HasFactory;

    protected $table = "user_profile";
    protected $primary = "id";

    protected $fillable = [
        'user_id',
        'jawatan',
        'gred',
        'bahagian'
    ];

    public function user()
    {
        //return $this->belongsTo(User::class, 'nama field dalam table userprofile', 'nama field dalam table User' );

        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
