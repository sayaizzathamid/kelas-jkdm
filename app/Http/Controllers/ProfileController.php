<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserProfile;

class ProfileController extends Controller
{
    public function papar_profil()
    {
        // $senarai_pengguna = User::query()->where('peranan','!=','admin')->where('id',1)->orWhere()->whereHas('userprofile')->get(); 
        // contoh2 where

        $senarai_pengguna = User::query()->get();
        $nombor = "Satu";

        //dd($senarai_pengguna);

        return view('pengurusan_profil.papar_profil', compact('senarai_pengguna','nombor'));
    }

    public function kemaskini_profil()
    {
        $senarai_pengguna = User::query()->get();
        $nombor = "Satu";

        //dd($senarai_pengguna);
        return view('pengurusan_profil.kemaskini_profil', compact('senarai_pengguna','nombor'));
    }

    public function kemaskini_profil_simpan(Request $request)
    {
        //dd($request->all());

        $update_pengguna = User::query()->where('id', $request->id)->update([
            'nama_penuh' => $request->nama_penuh,
            'emel' => $request->emel,
            'mykad' => $request->mykad,
            'no_fon' => $request->no_fon,
            'peranan' => $request->peranan
        ]);

        $check_user_profile = UserProfile::query()->where('user_id', $request->id)->first();

        if($check_user_profile)
        {
            $check_user_profile->update([
                'user_id' => $request->id,
                'bahagian' => $request->bahagian,
                'jawatan' => $request->jawatan,
                'gred' => $request->gred
            ]);
        }
        else
        {
            $create_user_profile = UserProfile::query()->create([
                'user_id' => $request->id,
                'bahagian' => $request->bahagian,
                'jawatan' => $request->jawatan,
                'gred' => $request->gred
            ]);
        }
        return redirect()->route('kemaskini.profil');
    }

    public function padam_profil($id)
    {
        $id = decrypt($id);

        $user_profile = UserProfile::query()->where('user_id', $id)->first();
        if($user_profile)
        {
            $user_profile->delete();
        }

        $user = User::query()->where('id', $id)->delete();

        return redirect()->route('kemaskini.profil');
    }
}
