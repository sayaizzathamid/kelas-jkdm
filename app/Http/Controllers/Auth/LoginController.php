<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    //
    public function login_auth(Request $request)
    {
        //dd($request->all());
        
        $credentials = $request->only('emel', 'password'); // guna emel
        //$credentials = $request->only('mykad', 'password'); // guna mykad
        if (Auth::attempt($credentials)) {
            return redirect()->route('view.dashboard');
        }
        else{
            return redirect()->route('login');
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('page.welcome');
    }
}
