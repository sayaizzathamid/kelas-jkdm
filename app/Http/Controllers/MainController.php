<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; //nota: untuk request input dari form
use App\Models\User; // nota: untuk connect dengan table DBS
use Hash;
use Mail;
use App\Mail\PengesahanPendaftaran;

class MainController
{
	public function welcome()
	{
		$nama_pengguna = "Izzat";
		return view('main.welcome', compact('nama_pengguna'));
	}

	public function login(){
	    //$no_ic = decrypt($ic); 
	    return view('login');
	}

	public function register()
	{
		return view('register');
	}

	public function register_store(Request $request)
	{
		//dd($request->all());

		$store = User::query()->create([
			'nama_penuh' => $request->nama_penuh,
			'emel' => $request->emel_rasmi,
			'no_fon' => $request->no_fon,
			'mykad' => $request->mykad,
			'password' => Hash::make('123'),
			'peranan' => 'admin'
		]);

		Mail::to($request->emel_rasmi)->send(new PengesahanPendaftaran([
			'emel' => $request->emel_rasmi,
			'katalaluan' => '123'
		]));

		return redirect()->route('page.welcome');
	}
}