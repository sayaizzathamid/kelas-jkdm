<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ env('APP_NAME') }}</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('fontawesome/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen   animated fadeInDown">
        <div>
            <div>

                <img src="{{ asset('img/logo.png') }}" style="width: 40%;">

            </div>
            <br>
            <h3>Pendaftaran Pengguna {{ env('APP_NAME') }}</h3>
            <br>
            <form class="m-t" role="form" action="{{ route('register.store') }}" method="POST">
                @csrf

                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Nama Penuh" required="" name="nama_penuh">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="E-mel Rasmi" required="" name="emel_rasmi">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Nombor MyKad" required="" name="mykad">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Nombor Telefon" required="" name="no_fon">
                </div>
                <div class="form-group">
                        <div class="checkbox i-checks"><label> <input type="checkbox"><i></i>&nbsp;&nbsp;Agree the terms and policy </label></div>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Daftar Akaun</button>

                <p class="text-muted text-center"><small>Telah mempunyai akaun?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="{{ route('login') }}">Log Masuk</a>
            </form>
            @php
                $year = date('Y');
            @endphp
            <p class="m-t"> <small>{{ env('APP_FOOTER') }} &copy; {{ $year }}</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
</body>

</html>
