@extends('layouts.master')
@section('content')
	<table class="table">
		<tr>
			<th>Bil</th>
			<th>Nama Pengguna</th>
			<th>Emel Pengguna</th>
			<th>Peranan Pengguna</th>
			<th>Nombor Telefon</th>
			<th>Aktiviti</th>
		</tr>
		@foreach($senarai_pengguna as $sp)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $sp->nama_penuh }}</td>
				<td>{{ $sp->emel }}</td>
				<td>{{ $sp->peranan }}</td>
				<td>{{ $sp->no_fon }}</td>
				<td>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal-{{$sp->id}}">
                        <i class="fa fa-info-circle fa-2x" style="color: black;"></i>
                    </button>
					<div class="modal inmodal" id="myModal-{{$sp->id}}" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content animated bounceInRight">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span aria-hidden="true">&times;</span><span class="sr-only">Tutup</span>
									</button>
									<i class="fa fa-info-circle modal-icon"></i>
									<h4 class="modal-title">
										Maklumat Terperinci
									</h4>
									<!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
								</div>
								<div class="modal-body">
									<div class="form-group">
										<label>Nama Penuh</label>
										<input type="text" class="form-control" value="{{ $sp->nama_penuh }}">
									</div>
									<div class="form-group">
										<label>Nombor MyKad</label>
										<input type="text" class="form-control" value="{{ $sp->mykad }}">
									</div>
									<div class="form-group">
										<label>E-mel</label>
										<input type="text" class="form-control" value="{{ $sp->emel }}">
									</div>
									<div class="form-group">
										<label>Nombor Telefon</label>
										<input type="text" class="form-control" value="{{ $sp->no_fon }}">
									</div>
									<div class="form-group">
										<label>Peranan</label>
										<input type="text" class="form-control" value="{{ $sp->peranan }}">
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
									<!-- <button type="button" class="btn btn-primary">Save changes</button> -->
								</div>
							</div>
						</div>
					</div>
				</td>
			</tr>
		@endforeach
	</table>
@endsection