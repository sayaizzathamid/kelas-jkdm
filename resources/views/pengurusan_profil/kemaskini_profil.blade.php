@extends('layouts.master')
@section('content')
<table class="table table-borderless">
	<thead>
		<tr>
			<th>Bil</th>
			<th>Nama Pengguna</th>
			<th>Emel Pengguna</th>
			<th>Peranan Pengguna</th>
			<th>Nombor Telefon</th>
			<th>Aktiviti</th>
		</tr>
	</thead>
	<tbody>
		@foreach($senarai_pengguna as $sp)
		<tr>
			<td>{{ $loop->iteration }}</td>
			<td>{{ $sp->nama_penuh }}</td>
			<td>{{ $sp->emel }}</td>
			<td>{{ $sp->peranan }}</td>
			<td>{{ $sp->no_fon }}</td>
			<td>
				<!--Kod Popup borang kemaskini -->
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal-{{$sp->id}}">
					<i class="fa fa-edit fa-2x" style="color: white;"></i>
				</button>
				<div class="modal inmodal" id="myModal-{{$sp->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content animated bounceInLeft">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">
									<span aria-hidden="true">&times;</span><span class="sr-only">Tutup</span>
								</button>
								<i class="fa fa-edit modal-icon" style="color: darkblue;"></i>
								<h4 class="modal-title">
									Kemaskini Maklumat Terperinci
								</h4>
								<!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
							</div>
							<form role="form" action="{{route('kemaskini.profil.simpan')}}" method="POST">
								@csrf
								<div class="modal-body">
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label>ID Pengguna</label>
												<input type="text" class="form-control" value="{{ $sp->id }}" name="id" readonly>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label>Nombor MyKad</label>
												<input type="text" class="form-control" value="{{ $sp->mykad }}" name="mykad">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label>Nama Penuh</label>
												<input type="text" class="form-control" value="{{ $sp->nama_penuh }}" name="nama_penuh">
											</div>
										</div>
										<div class="col-lg-6">	
											<div class="form-group">
												<label>E-mel</label>
												<input type="text" class="form-control" value="{{ $sp->emel }}" name="emel">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label>Nombor Telefon</label>
												<input type="text" class="form-control" value="{{ $sp->no_fon }}" name="no_fon">
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label>Peranan</label>
												<!-- <input type="text" class="form-control" value="{{-- $sp->peranan --}}" name="peranan"> -->
												<select class="form-control" name="peranan">
													<option value="">--SILA PILIH--</option>
													<option value="pengguna" @if($sp->peranan == 'pengguna') selected @endif>Pengguna</option>
													<option value="admin" @if($sp->peranan == 'admin') selected @endif>Admin</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label>Jawatan</label>
												<input type="text" class="form-control" value="{{ $sp->userprofile->jawatan ?? '' }}" name="jawatan">
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label>Gred</label>
												<input type="text" class="form-control" value="{{ $sp->userprofile->gred ?? '' }}" name="gred">
											</div>
										</div>
									</div>
									<div class="form-group">
										<label>Bahagian</label>
										<input type="text" class="form-control" value="{{ $sp->userprofile->bahagian ?? '' }}" name="bahagian">
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
									<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>  Kemaskini</button>
								</div>
							</form>
						</div>
					</div>
				</div>

				<!--Kod pop pengesahan sebelum padam-->
				<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#padam-{{$sp->id}}">
					<i class="fa fa-trash fa-2x" style="color: white;"></i>
				</button>
				<div class="modal inmodal" id="padam-{{$sp->id}}" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-md">
						<div class="modal-content animated flipInX">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">
									<span aria-hidden="true">&times;</span><span class="sr-only">Tutup</span>
								</button>
								<i class="fa fa-question-circle modal-icon" style="color: darkred;"></i>
								<h4 class="modal-title">
									Pengesahan
								</h4>
								<!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
							</div>
							<div class="modal-body">
								<div class="col-lg-12">
									<h2>
										Anda pasti untuk padam maklumat pengguna seperti dibawah?
									</h2>
								</div>
								<br>
								<div class="col-lg-12">
									<table class="table" style="font-size: 15px;">
										<tbody>
											<tr>
												<th>Nama</th>
												<th>:</th>
												<td>{{ $sp->nama_penuh }}</td>
											</tr>
											<tr>
												<th>MyKad</th>
												<th>:</th>
												<td>{{ $sp->mykad }}</td>
											</tr>
											<tr>
												<th>E-mel</th>
												<th>:</th>
												<td>{{ $sp->emel }}</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
								<a href="{{ route('padam.profil',encrypt($sp->id)) }}">
									<button class="btn btn-danger">
										<i class="fa fa-trash"></i> Padam
									</button>
								</a>
							</div>
						</div>
					</div>
				</div>
				
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@endsection