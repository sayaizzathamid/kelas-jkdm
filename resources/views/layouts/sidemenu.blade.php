<nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <img alt="image" class="rounded-circle" src="{{ asset('img/profile_small.jpg') }}"/>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="block m-t-xs font-bold">{{ Auth::user()->nama_penuh }}</span>
                                <span class="text-muted text-xs block">{{ Auth::user()->userprofile->jawatan }} <b class="caret"></b></span>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a class="dropdown-item" href="profile.html">Profile</a></li>
                                <li><a class="dropdown-item" href="contacts.html">Contacts</a></li>
                                <li><a class="dropdown-item" href="mailbox.html">Mailbox</a></li>
                                <li class="dropdown-divider"></li>
                                <li><a class="dropdown-item" href="{{ route('logout') }}">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            IN+
                        </div>
                    </li>
                    <li>
                        <a href="{{ route('view.dashboard') }}"><i class="fa fa-user"></i> <span class="nav-label">Dashboard Saya</span></a>
                    </li>
                    <li class="">
                        <a href="index.html"><i class="fa fa-th-large"></i> <span class="nav-label">Pengurusan Profil</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="{{ route('papar.profil') }}">Papar Profil</a></li>
                            <li><a href="{{ route('kemaskini.profil') }}">Kemaskini Profil</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>