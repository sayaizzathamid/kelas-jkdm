<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ env('APP_NAME') }}</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('fontawesome/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <!-- <h1 class="logo-name">IN+</h1> -->
                <img src="{{ asset('img/logo.png') }}" style="width: 40%;">
            </div>
            <h3>Selamat Datang ke {{ env('APP_NAME') }}</h3>
            <p>
                Gerbang maklumat perkhidmatan pegawai
            </p>
            <hr>
            <p>Pengesahan Pengguna</p>
            <form class="m-t" role="form" action="{{ route('login.auth') }}" method="POST">
                @csrf
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Emel Pengguna" required="" name="emel">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Katalaluan" required="" name="password">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b" style="background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(121,111,9,1) 0%, rgba(0,212,255,1) 100%);">Log Masuk</button>

                <a href="#"><small>Lupa katalaluan?</small></a>
                <p class="text-muted text-center"><small>Belum daftar akaun?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="{{ route('register') }}">Daftar akaun</a>
            </form>
            @php
                $year = date('Y');
            @endphp
            <p class="m-t"> <small>{{ env('APP_FOOTER') }} &copy; {{ $year }}</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>

</body>

</html>
