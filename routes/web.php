<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('main.welcome');
// }); //nota : return direct view dari Route

Route::get('/', [MainController::class,'welcome'])->name('page.welcome'); // nota : return view melalui controller

// Route::get('login/{ic}', function($ic){
//     $no_ic = decrypt($ic); 
//     return view('login', compact('no_ic'));
// })->name('login'); // nota: decrypt kat ROUTE 

//ROUTE LOGIN
Route::get('login',[MainController::class, 'login'])->name('login'); // nota: decrypt kat MainController
Route::post('login/auth', [LoginController::class, 'login_auth'])->name('login.auth');


//ROUTE PENDAFTARAN AKAUN
Route::get('register', [MainController::class, 'register'])->name('register');
Route::post('register/store', [MainController::class, 'register_store'])->name('register.store');

Route::get('dashboard', function(){
		return view('layouts.master');
	})->middleware('auth'); // opt1 : set middleware setiap route

Route::group(['middleware' => ['auth']], function(){
	Route::get('dashboard', function(){
		return view('layouts.master');
	});

	Route::get('view/dashboard', function(){
		return view('dashboard');
	})->name('view.dashboard');

	Route::get('logout', [LoginController::class, 'logout'])->name('logout');

	Route::get('papar/profil', [ProfileController::class, 'papar_profil'])->name('papar.profil');
	Route::get('kemaskini/profil', [ProfileController::class, 'kemaskini_profil'])->name('kemaskini.profil');
	Route::post('kemaskini/profil/simpan', [ProfileController::class, 'kemaskini_profil_simpan'])->name('kemaskini.profil.simpan');

	Route::get('padam/profil/{id}', [ProfileController::class, 'padam_profil'])->name('padam.profil');
});// opt2 : set middleware by group 
